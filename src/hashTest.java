// apache-commons-codec-1.4.jar
package tttte;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class hashTest {
	
	
	public static final String sha512Base64(String val) {
		byte[] hash = DigestUtils.sha512(val);
		String base64 = Base64.encodeBase64String(hash);
		return base64;
	}
	
 public static void main(){
	 String salt =  RandomStringUtils.randomAlphabetic(30);
		String hash = sha512Base64("g1234567" + salt);
		
		System.out.println("'" + salt+"','+"+hash+"'"); 
 }
}
