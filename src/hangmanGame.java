package tttte;

import javax.swing.JOptionPane;

public class hangmanGame {

	// Where the user has to try to guess a letter inside of a word and if the
	// letter is wrong they get an X
	// and after 6 X's they lose and the game is over.
	// BUT it has to remember each letter they get correct and
	// let them know the spot there in such as if they guessed a T and
	// the word was titan it would show t _ t _ _
 
	public static String hangman(String str_word, String str_guess, String str_input) {
		System.out.println("\nGuess: " + str_input + "\n");

		char[] charWordArray = str_word.toCharArray();
		char[] charGuessArray = str_guess.toCharArray();

		int guesslength = 0;

		int counttry = 0;

		String str_guess1 = str_guess;
		for (int i = 0; i < charWordArray.length; i++) {

			if (str_input.equals(String.valueOf(charWordArray[i]))) {

				guesslength = i * 2;
				str_guess = str_guess.substring(0, guesslength + 1) + String.valueOf(charWordArray[i])
						+ str_guess.substring(guesslength + 2, str_guess.length());
			}
		}

		if (str_guess.equals(str_guess1)) {
			str_guess += " X";
		}

		for (int i = 0; i < str_guess.length(); i++) {
			if (str_guess.charAt(i) == 'X') {
				counttry++;
			}
		}

		System.out.println(str_guess);

		String str_guess2 = str_guess.replace(" ", "");

		str_guess2 = str_guess2.replace("X", "");

		String str_output = "";

		if (str_guess2.equals(str_word) && counttry != 6) {
			// System.out.println("Game Win");
			str_output = "Game Win";
            str_guess = "win";
			
		} else if (!str_guess2.equals(str_word) && counttry == 6) {

			str_guess = "end";
			str_output += "\nGame Over";
			str_output += "\nAnswer = " + str_word;
 
		}
		
		if(str_guess.equals("win")||str_guess.equals("end")){
		    JOptionPane.showMessageDialog(null, str_output, "Output", JOptionPane.INFORMATION_MESSAGE);

			int selectedOption = JOptionPane.showConfirmDialog(null, "Continue?", "Choose", JOptionPane.YES_NO_OPTION);

			if (selectedOption == JOptionPane.YES_OPTION) {
				gameStart();
			} else {
				System.exit(0);
			}
		}

		return str_guess;

	}

	public static void gameStart() {
		String str_word = "baacdw";
		String str_guess = " _";

		for (int i = 0; i < str_word.length() - 1; i++) {
			str_guess += " _";
		}
 
		while (str_guess != "end") {

			String str_input = JOptionPane.showInputDialog(str_guess + "\nHangman - guess\n");

			str_guess = hangman(str_word, str_guess, str_input);
		}
	}

	public static void main(String args[]) {

		gameStart();

	}
}
