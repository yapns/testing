//import java.io.StringReader;
package tttte;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils; 

public class createCurrencyCSV {

	private static String getWebPabeSource(String sURL) throws IOException {
		URL url = new URL(sURL);
		URLConnection urlCon = url.openConnection();
		BufferedReader in = null;

		if (urlCon.getHeaderField("Content-Encoding") != null
				&& urlCon.getHeaderField("Content-Encoding").equals("gzip")) {
			in = new BufferedReader(new InputStreamReader(new GZIPInputStream(urlCon.getInputStream())));
		} else {
			in = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
		}

		String inputLine;
		StringBuilder sb = new StringBuilder();

		while ((inputLine = in.readLine()) != null)
			sb.append(inputLine);
		in.close();
        
		return sb.toString();
	}

	public static String getWebHTML(String str_url) throws IOException {
		// Read Web HTML
		String a = getWebPabeSource(str_url);
		String b = a.replace(" ", "\n");

		// Filter get contents of TblHdr
		String c = null;
		
		if (b.contains("TblHdr")){
			c = b.substring(b.indexOf("TblHdr"), b.indexOf("/table"));
			c = c.replaceAll("\n", "");
			c = c.replace("</th>", "\n");
			c = c.replace("</tr>", "\n");
			c = c.replace("\r", "");
		}
	
		return c;
	}

	public static String getLatestDate(String str_html) {
		// Get table data
		String[] tds = StringUtils.substringsBetween(str_html, "<td>", "</td>");

		// Get latest date
		String str_latest = null;
		
		String datePattern1 = "\\d{1}/\\d{2}/\\d{4}";
		String datePattern2 = "\\d{2}/\\d{2}/\\d{4}";
		String datePattern3 = "\\d{1}/\\d{1}/\\d{4}";
		String datePattern4 = "\\d{2}/\\d{1}/\\d{4}";

		Boolean isDate1 ;
		Boolean isDate2 ;
		Boolean isDate3 ;
		Boolean isDate4 ;
		
		int j = 0;
		for (String td : tds) {
			
			isDate1 = td.matches(datePattern1);
			isDate2 = td.matches(datePattern2);
			isDate3 = td.matches(datePattern3);
			isDate4 = td.matches(datePattern4);
			
			if (isDate1 || isDate2 || isDate3 || isDate4) {
				str_latest = td;
			}
		}

		return str_latest;
	}

	public static ArrayList<String> getTableHeader(String str_html) {
		// Get table header
		ArrayList<String> thlist = new ArrayList<String>();

		String[] ths = StringUtils.substringsBetween(str_html, "<th><b>", "</b>");
		for (String th : ths) {

			if (!th.equals("")) {
				thlist.add(th);
			}
		}
		return thlist;
	}

	public static ArrayList<String> getTableData(String str_html, String str_latest) {

		// Get date data
		String[] lines1;
		String regex = "\\n";
		lines1 = str_html.split(regex);

		ArrayList<String> tdlist = new ArrayList<String>();

		for (int i = 0; i < lines1.length; i++) {
			if (lines1[i].contains(str_latest)) {
			 
				String[] tds1 = StringUtils.substringsBetween(lines1[i], "<td>", "</td>");
				for (String td1 : tds1) {

					if (!td1.equals(str_latest)) {
						tdlist.add(td1);
					}
				}
			}
		}
		return tdlist;
	}

	public static void createCSV(String str_filename, String str_latestDate, ArrayList<String> thlist, ArrayList<String> tdlist)
			throws FileNotFoundException {
		
	 
			PrintWriter pw = new PrintWriter(new File(str_filename));
			StringBuilder sb = new StringBuilder();
			sb.append("CurrencyCode");
			sb.append(',');
			sb.append("Unit");
			sb.append(',');
			sb.append("CurrencyRate");
			sb.append(',');
			sb.append("Date");
			sb.append('\n');

			String str_th ="";
		  
			String str_unit;
			
			for (int i = 0; i < tdlist.size(); i++) {
		 
				if(thlist.get(i).length()>3){
				str_th = thlist.get(i).substring(0,3); 
				str_unit ="100";
			    }else{
				str_th =thlist.get(i); 
				str_unit ="1";
			    }
			    sb.append(str_th);
			    sb.append(',');
			    sb.append(str_unit);
			    sb.append(',');
			    sb.append(tdlist.get(i));
			    sb.append(',');
				sb.append(str_latestDate);
			    sb.append('\n');
			}

			pw.write(sb.toString());
			pw.close();
			System.out.println("CSV created!"); 
	}

	public static void main(String[] args) throws Exception {
		
		String str_url = "http://www.bnm.gov.my/index.php?ch=statistic&pg=stats_exchangerates";
        String str_filename = "test2.csv";
        
		String str_html = getWebHTML(str_url);
		
		if (str_html != null){
		   String str_latestDate = getLatestDate(str_html);

		   ArrayList<String> thlist = getTableHeader(str_html);
		   ArrayList<String> tdlist = getTableData(str_html, str_latestDate);
		   createCSV(str_filename,str_latestDate, thlist, tdlist);
		   
		}else{
			System.out.println("Invalid URL");
		}
	}
}