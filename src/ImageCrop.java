import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.*;

class ImageCrop {

    public static int countAll(File directory){
        int count =0;
        for (File file: directory.listFiles()){
            if (file.isFile()){
                count++;
            }
            if (file.isDirectory()){
                count += countAll(file);
            }
        }
        return count;
    }
    
    public static void main(String[] args) throws Exception {
    	Image inputimage = null;
        
        String mainfolder ="C://Users//naisiong.yap//Pictures//input";
        String str_inputfilename="C://Users//naisiong.yap//Pictures//input//test";
        String str_outputfilename="C://Users//naisiong.yap//Pictures//output//out";
        
        File folder = new File(mainfolder);
        int totalfiles =countAll (folder);
        System.out.println("count= "+  totalfiles);

        try {
            
            for (int i=0;i< totalfiles;i++){
            File sourceimage = new File(str_inputfilename+ i +".jpg");
            
            inputimage = ImageIO.read(sourceimage);
            BufferedImage bufferedInput = (BufferedImage) inputimage; 
            
            
//getSubimage(x,y,x + width,y + height); Parameters:
//x - the X coordinate of the upper-left corner of the specified rectangular region
//y - the Y coordinate of the upper-left corner of the specified rectangular region
//w - the width of the specified rectangular region
//h - the height of the specified rectangular region
            
            final Image outputimage= bufferedInput.getSubimage(0, 50, bufferedInput.getWidth(), bufferedInput.getHeight()-200);
            BufferedImage bufferedOutput = (BufferedImage) outputimage;
            
            ImageIO.write(bufferedOutput, "jpg",new File(str_outputfilename+ i +".jpg"));
            }   
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }
}