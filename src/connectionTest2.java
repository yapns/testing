package test2;

//Java Build Path > sqljdbc4.jar

import java.sql.Connection; 
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class connectionTest2 {

	public static int textLength = 12;
	
	public static void printTable(String str){
	 	 
			if(str.length() < textLength){
				int diff = textLength - str.length();

				for(int j=0;j<diff;j++){
					str += " ";
				}
			}
		  System.out.print(" | "+ str);
	}
	
	public static void main(String[] args) {
		 
		try {
			System.out.println("Start connect \n");
			 
			//DB2
			//com.ibm.db2.jcc.DB2Driver
            //db2jcc4.jar
            //jdbc:db2://hla451:50000/E2PC

            //Mysql 
			//com.mysql.jdbc.Driver
			//jdbc:mysql://localhost:3306/pms
			
            //MSSQL
			//com.microsoft.sqlserver.jdbc.SQLServerDriver
			//sqljdbc4.jar
			//jdbc:sqlserver://10.0.1.171:1433;databaseName=VMAEDM
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		} catch (ClassNotFoundException cnfe) {
			System.out.println("ClassNotFoundException");
		}

		Connection conn = null;

		try {

			String dbURL = "jdbc:sqlserver://10.0.1.171:1433;databaseName=VMAEDM";
			String user = "sa";
			String pass = "password";
			conn = DriverManager.getConnection(dbURL, user, pass);
			if (conn != null) {
//				DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
//				System.out.println("Driver name: " + dm.getDriverName());
//				System.out.println("Driver version: " + dm.getDriverVersion());
//				System.out.println("Product name: " + dm.getDatabaseProductName());
//				System.out.println("Product version: " + dm.getDatabaseProductVersion() + "\n");

				Statement statement = conn.createStatement();
				String queryString = "select * from vmaedm.dbo.d_dmuser";
				
				System.out.println(queryString+"\n");
				
				ResultSet rs = statement.executeQuery(queryString);
				
				ResultSetMetaData rsmd = rs.getMetaData();
				
				int columnCount = rsmd.getColumnCount();
 
				for (int i = 1; i <= columnCount; i++ ) { 
					String strColumn = rsmd.getColumnName(i);
					printTable( strColumn ); 
				}
				System.out.print("|\n");
				 
				while (rs.next()) {
 
					for (int i = 1; i <= columnCount; i++ ) { 
						String strResult = rs.getString(i);
						printTable( strResult ); 
					}
					System.out.print("|\n");
				}
				
				System.out.print("\n");
				
			} else {
				System.out.println("Failed to connect ");
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
					System.out.println("Close connect ");
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

}

//Output:
//
//Start connect 
//
//select * from vmaedm.dbo.d_dmuser
//
// | DMUser_key   | Userid       | UserName     | DisplayName  | NeedsUpdate |
// | 0            | -1           | Unknown      | Unknown      | 0           |
// | 1            | 0            | (NONE)       | (NONE)       | 0           |
//
//Close connect 