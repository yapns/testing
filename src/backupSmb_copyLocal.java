package question;

import java.io.File;
import java.io.FileInputStream; 
import java.io.IOException; 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

//jcifs-1.3.19.jar
import jcifs.smb.NtlmPasswordAuthentication; 
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

public class test7 {

	public static String strTodayDate = getTodayDate();

	public static String url = "smb://10.0.1.171/c$/Program Files/IBM/WebSphere/AppServer/flex/";
	public static String str_usr = "admin";
	public static String str_pwd = "password";
 
	public static String getTodayDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();

		String strTodayDate = dateFormat.format(cal.getTime());

		return strTodayDate;
	}

	public static void copyLocalFile(String strSmbFolder, String strLocalFolder, String strChangedFile) throws IOException {

		System.out.println("\nCopying file from local ....");
		
		String sourcePath = strLocalFolder + strChangedFile;
		String destinationPath = strSmbFolder + strChangedFile;
		
		NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, str_usr, str_pwd);
		SmbFile sFile = new SmbFile(destinationPath, auth);

		SmbFileOutputStream smbFileOutputStream = new SmbFileOutputStream(sFile);

		FileInputStream fileInputStream = new FileInputStream(new File(sourcePath));

		final byte[] buf = new byte[16 * 1024 * 1024];
		int len;
		while ((len = fileInputStream.read(buf)) > 0) {
			smbFileOutputStream.write(buf, 0, len);
		}
		fileInputStream.close();
		smbFileOutputStream.close();
		System.out.println("Copied, Done !!! file:"+strChangedFile+"\n=======================\n");
	}
 
	public static void backupSmbFile (String strSmbFolder, String strChangedFile) {
	
		String strfilename;
		try {
			System.out.println("Connecting....");
			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("", str_usr, str_pwd);
			SmbFile source = new SmbFile(strSmbFolder, auth);

			for (SmbFile f : source.listFiles()) {
				strfilename = f.getUncPath();

				strfilename = strfilename.substring(strSmbFolder.replace("smb:", "").lastIndexOf("/") + 1,
						strfilename.length());

				if (strfilename.equals(strChangedFile)) {

					// START Backup
					System.out.println("\nCheck Backup ....");
					String strbackupFile = strfilename + "_" + getTodayDate();
					SmbFile newFile = new SmbFile(strSmbFolder + strbackupFile, auth);
					Boolean backupFileExist = false;

					for (SmbFile f1 : source.listFiles()) {
						String str = f1.getUncPath();
						str = str.substring(str.lastIndexOf("\\") + 1, str.length());

						if (str.equals(strbackupFile)) {
							backupFileExist = true;
						}
					}

					if (backupFileExist) {
						System.out.println("Backup Exist!!! file:" + strbackupFile);
					} else {
						f.renameTo(newFile);
						System.out.println("Backup Done!!! file:" + strbackupFile);
					}
					// End Backup
				}
			}

		} catch (Exception e) {
			System.out.println("Logon failure: unknown user name or bad password.");
			System.out.println("Please try again.");
		}
	}

	public static void main(String[] args) throws IOException {

		 String smbFolder1 = url + "test/";
		 String localFolder1 = "C:/Users/Yap/Documents/";
		 String changedFile1 = "test.txt";
		 
		 backupSmbFile(smbFolder1, changedFile1);
		 copyLocalFile(smbFolder1, localFolder1,  changedFile1);

	}

}
