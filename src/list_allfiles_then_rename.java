import java.io.File;
import javax.swing.JOptionPane;
import org.apache.commons.io.FilenameUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nsyap
 */
public class list_allfiles_then_rename {

    public static void rename(final File folder, String str_folder) {
        String str = "";
        String str_extension = "";
        String str_newfilename = "";

        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, str_folder);
            } else {
                str_extension = FilenameUtils.getExtension(fileEntry.getName());
                str_newfilename = fileEntry.getName().replaceAll("[^0-9]+", "") + "." + str_extension;

                str = str + fileEntry.getName() + " rename to " + str_newfilename + "\n";
                //System.out.println(fileEntry.getName() + " rename to " + str_newfilename);

                File file = new File(str_folder + "\\" + fileEntry.getName());
                File newFile = new File(str_folder + "\\" + str_newfilename);
                file.renameTo(newFile);
            }
        }

    }

    public static void listFilesForFolder(final File folder, String str_folder) {
        String str = "";
        String str_extension = "";
        String str_newfilename = "";

        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, str_folder);
            } else {
                str_extension = FilenameUtils.getExtension(fileEntry.getName());
                str_newfilename = fileEntry.getName().replaceAll("[^0-9]+", "") + "." + str_extension;

                str = str + fileEntry.getName() + " rename to " + str_newfilename + "\n";
                System.out.println(fileEntry.getName() + " rename to " + str_newfilename);
            }
        }
        JOptionPane.showMessageDialog(
                null, str, "Output", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void main(String[] args) {

        String str = JOptionPane.showInputDialog("Enter your folder path:");
        final File folder = new File(str);
        listFilesForFolder(folder, str);

        int selectedOption = JOptionPane.showConfirmDialog(null, "Want to rename?", "Choose", JOptionPane.YES_NO_OPTION);

        if (selectedOption == JOptionPane.YES_OPTION) {
            rename(folder, str);
        } else {

        }
    }

}
