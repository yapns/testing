https://howtodoinjava.com/scripting/jquery/jquery-detect-if-enter-key-is-pressed/

    <% 
        if(request.getParameter("buttonName") != null) {
               session.setAttribute("status", "guest");
        }
    %>

    <FORM NAME="form1" METHOD="POST">
        <INPUT TYPE="HIDDEN" NAME="buttonName">
        <INPUT TYPE="BUTTON" VALUE="Button 1" ONCLICK="button1()">
    </FORM>

    <SCRIPT LANGUAGE="JavaScript">
        <!--
        function button1()
        {
            document.form1.buttonName.value = "yes";
            form1.submit();
        } 
        // --> 
    </SCRIPT>
	
	
	<html>
    <head>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <h1>Detect ENTER key with jQuery</h1>
        <label>TextBox Area: </label>
        <input id="someTextBox" type="text" size="40" />
        <script type="text/javascript">
            //Bind keypress event to textbox
            $('#someTextBox').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    alert('You pressed a "enter" key in textbox'); 
                }
                //Stop the event from propogation to other handlers
                //If this line will be removed, then keypress event handler attached
                //at document level will also be triggered
                event.stopPropagation();
            });
             
            //Bind keypress event to document
            $(document).keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    alert('You pressed a "enter" key in somewhere');   
                }
            });
        </script>
    </body>
</html>