
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author YapNS
 */
public class Question3 {

    public static boolean isNumeric(String str) {
        try {
            int d = Integer.parseInt(str);

            if (d < 0) {
                return false;
            }
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static String listString(ArrayList<String> list) {

        CharSequence[] cs = list.toArray(new CharSequence[list.size()]);

        String str = Arrays.toString(cs);
        str = str.replace("[", " ");
        str = str.replace(", ", " ");
        str = str.replace(" ", "");
        str = str.replace("]", "");

        return str;
    }

    public static char[] cyclicSort(char[] char_input, int ordering) {

        ArrayList<String> input_list  = new ArrayList<>();  // input list to store input data
        ArrayList<String> output_list = new ArrayList<>();  //output list to store output data
        ArrayList<String> temp_list   = new ArrayList<>();  //  temp list to store processing data

        for (char temp : char_input) {
            String str_char;
            str_char = String.valueOf(temp);
            input_list.add(str_char);  //input list add all char of input
        }

        int loop = 0;
        int count = 0;
        int char_num = 0;

        String str_remain = "";

        for (int i = 0; i < input_list.size(); i++) {
            str_remain += input_list.get(i);
            if (i != input_list.size() - 1) {
                str_remain += ", ";
            }
        }
        System.out.println("List: " + str_remain + "\n");

        while (input_list.size() > 1) {
            //reset remaining char to null
            str_remain = "";

            //count how many repeat loop based on ordering
            for (int n = 1; n < ordering; n++) {
                if (n * input_list.size() > ordering) {
                    loop = n;
                    break;
                }
            }

            for (int i = 0; i < loop; i++) {
                for (int j = 0; j < input_list.size(); j++) {
                    count++;

                    if (count == ordering) { //find count for repeat loop the input list n times which equal to ordering
                        char_num = j;
                    }
                }
            }

            System.out.println("Sort out [" + ordering + "] = " + input_list.get(char_num));

            if (char_num == 0) {
                for (int i = char_num + 1; i < input_list.size(); i++) {
                    temp_list.add(input_list.get(i));    //temp list add in (next value) from input list
                }
            } else {
                for (int i = char_num + 1; i < input_list.size(); i++) {
                    temp_list.add(input_list.get(i));    //temp list add in (next value) from input list
                }

                for (int i = 0; i < char_num; i++) {
                    temp_list.add(input_list.get(i));     // temp list add in (value before) from input list
                }
            }

            output_list.add(input_list.get(char_num));   //output list add in selected ordering from input list

            input_list.clear(); //clear input list
            input_list.addAll(temp_list); //save to input list from temp list

            for (int i = 0; i < input_list.size(); i++) {
                str_remain += input_list.get(i);
                if (i != input_list.size() - 1) {
                    str_remain += ", ";
                }
            }
            System.out.println("Remaining: " + str_remain + "\n");

            temp_list.clear(); //clear temp list

            count = 0; //reset count to zero

        }

        output_list.add(input_list.get(0));//add last value of input list to output list
        System.out.println("Sort out [" + ordering + "] = " + input_list.get(0) + "\n");

        String str = listString(output_list);

        char[] charArray = str.toCharArray();

        return charArray;
    }

    public static void main(String[] args) {

// ==================== QUESTION 3 =============================
//  char[] input = "ABCDEFGHIJ".toCharArray();
//	char[] output = cyclicSort(input, 7);
//	System.out.println(new String(output));  // GDBACFJEHI
// ===========================================================
        String str_input = JOptionPane.showInputDialog("Input: ");

        char[] char_input = str_input.toCharArray();

        String num_input = JOptionPane.showInputDialog("Cyclic Sort with Ordering of : ");

        int ordering = 0;
        if (!isNumeric(num_input)) {

            JOptionPane.showMessageDialog(null,
                    "<html><body><p style='width: 200px;'>Please insert ordering with positive numeric input.</p></body></html>",
                    "Output", JOptionPane.INFORMATION_MESSAGE);
            main(args);

        } else {
            ordering = Integer.parseInt(num_input);

            char[] output = cyclicSort(char_input, ordering);

            //show output
            JOptionPane.showMessageDialog(null,
                    "<html><body><p style='width: 200px;'>Input : " + str_input + "</p><br/>"
                    + "<p>Cyclic Sort with Ordering of :" + num_input + "</p><br/>"
                    + "<p> Output is </p><p style='color:blue'>" + new String(output) + "</p></body></html>",
                    "Output", JOptionPane.INFORMATION_MESSAGE);

            //show continue option
            int selectedOption = JOptionPane.showConfirmDialog(null, "Continue?", "Choose", JOptionPane.YES_NO_OPTION);

            if (selectedOption == JOptionPane.YES_OPTION) {
                main(args);
            }
        }
    }
}
