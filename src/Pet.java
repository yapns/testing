
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Pet implements Comparable {

    int petId;
    String petType;

    public Pet(int argPetId, String argPetType) {
        petId = argPetId;
        this.petType = argPetType;
    }

    public int compareTo(Object o) {
        Pet petAnother = (Pet) o;
        return this.petType.compareTo(petAnother.petType);
    }

    public String toString() {
        return petType;
    }

    public static void main(String[] args) {
        List list = new ArrayList();
        list.add(new Pet(2, "Dog"));
        list.add(new Pet(1, "Parrot"));
        list.add(new Pet(2, "Cat"));

        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + "   " + list.get(i));
        }
        Collections.sort(list);
        for (Iterator iter = list.iterator(); iter.hasNext();) {
            Pet element = (Pet) iter.next();
            System.out.println(element);
        }
    }
}
