
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nsyap
 */
public class UnicodeHan {

    public static void main(String[] args) throws IOException {

        String infile = "雨夜花";

        System.out.println("Input  = " + infile);

        for (int j = 0; j < infile.length(); j++) {

            String char_at_i = Character.toString(infile.charAt(j));

            int codepoint = infile.codePointAt(j);

            //UnicodeHan_to_Decimal
            if (Character.UnicodeScript.of(codepoint) == Character.UnicodeScript.HAN) {

                String decimal = Integer.toString(codepoint);

                decimal = "&#" + decimal + ";";

                infile = infile.replace(char_at_i, decimal);
            }
        }

        System.out.println("Output = " + infile);

    }
}
