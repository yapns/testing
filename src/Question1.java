
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author YapNS
 */
public class Question1 {

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static int breakdown(char[] charTotal) {
        int sum = 0;
        int num = 0;

        for (char temp : charTotal) {

            String str_char;
            str_char = String.valueOf(temp);
            num = Integer.parseInt(str_char);
            sum = sum + num;
        }
        return sum;
    }

    public static String showOutput(String str_output, String str_char, int multiple, int num) {
        String str_equal = " = ";

        if (num < 10) {
            str_equal += "&nbsp;&nbsp;";
        }

        str_output = str_output + str_char + " => " + str_char + " * " + multiple + str_equal + num + "<br/>";

        return str_output;
    }

    public static void main(String[] args) {

        String str_input = JOptionPane.showInputDialog("Input: ");
        char[] charArray = str_input.toCharArray();

        String str_output = "<html><body><p style='width: 200px;'>";
        int total = 0;
        int num = 0;
        int count = 0;

        String str_total="";
        
        int multiple = 0;
        for (char temp : charArray) {

            String str_char;
            str_char = String.valueOf(temp);

            //check input whether all is numeric or not
            if (!isNumeric(str_char)) {

                JOptionPane.showMessageDialog(null,
                        "<html><body><p style='width: 200px;'>Please insert all numeric input.</p></body></html>",
                        "Output", JOptionPane.INFORMATION_MESSAGE);
                main(args);

            } else {

                num = Integer.parseInt(str_char);

                //mutiple the number based on its position
                for (int i = 0; i < str_input.length(); i++) {

                    if (count == 4 * i) {
                        multiple = 2;
                        num = num * multiple;
                        str_total += num + " + ";
                        str_output = showOutput(str_output, str_char, multiple, num);
                    }
                    if (count == (4 * i) + 1) {
                        multiple = 3;
                        num = num * multiple;
                        str_total += num + " + ";
                        str_output = showOutput(str_output, str_char, multiple, num);
                    }
                    if (count == (4 * i) + 2) {
                        multiple = 5;
                        num = num * multiple;
                        str_total += num + " + ";
                        str_output = showOutput(str_output, str_char, multiple, num);
                    }
                    if (count == (4 * i) + 3) {
                        multiple = 7;
                        num = num * multiple;
                        str_total += num + " + ";
                        str_output = showOutput(str_output, str_char, multiple, num);
                    }
                }

                total = total + num;
            }
            count++;
        }

        char[] charTotal = Integer.toString(total).toCharArray();

        String str_breakdown = "";

        int sum = 0;

        while (charTotal.length != 1) {

            sum = breakdown(charTotal);

            //3 digits breakdown
            if (charTotal.length == 3) {

                str_breakdown = str_breakdown + "<p>" + String.valueOf(charTotal) + " break down to "
                        + charTotal[0] + " + " + charTotal[1] + " + " + charTotal[2] + " = "
                        + sum + "</p><br/>";

            } //2 digits breakdown
            else if (charTotal.length == 2) {

                str_breakdown = str_breakdown + "<p>" + String.valueOf(charTotal) + " break down to " + charTotal[0] + " + " + charTotal[1]
                        + " = " + sum + "</p><br/>";

            }
            charTotal = Integer.toString(sum).toCharArray();
        }
    
        //show output
        JOptionPane.showMessageDialog(null,
                "<html><body><p style='width: 200px;'>Input= " + str_input + "</p><br/>"
                + str_output + "</p><br/>"
                + "<p style='color:blue'>Sum = " + str_total.substring(0,str_total.lastIndexOf("+")) + " = "+ total + "</p><br/>" + str_breakdown
                + "</p><p style='color:blue'>Check digit= " + sum + "</p></body></html>",
                "Output", JOptionPane.INFORMATION_MESSAGE);

        //show continue option
        int selectedOption = JOptionPane.showConfirmDialog(null, "Continue?", "Choose", JOptionPane.YES_NO_OPTION);

        if (selectedOption == JOptionPane.YES_OPTION) {
            main(args);
        } else {
            System.exit(0);
        }

    }

}
