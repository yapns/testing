
import java.math.BigInteger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author YapNS
 */
public class Question2 {

    public static BigInteger[] TribonacciPrime(int num) {
        BigInteger[] arrayTrib = new BigInteger[num];
        arrayTrib[0] = new BigInteger("2");
        arrayTrib[1] = new BigInteger("3");
        arrayTrib[2] = new BigInteger("4");

        BigInteger[] arrayTrib_Prime = new BigInteger[num];
        arrayTrib_Prime[0] = new BigInteger("2");
        arrayTrib_Prime[1] = new BigInteger("3");

        int j = 2;
        for (int i = 3; i < num; i++) {
            arrayTrib[i] = arrayTrib[i - 1].add(arrayTrib[i - 2].add(arrayTrib[i - 3]));  //Tribonacci Number

            if (isPrime(arrayTrib[i])) {  //Tribonacci Number AND Prime Number
                arrayTrib_Prime[j] = arrayTrib[i];
                j++;
            }
        }

        return arrayTrib_Prime;
    }

    public static boolean isPrime(BigInteger number) {
        if (!number.isProbablePrime(5)) {
            return false;
        }

        BigInteger two = new BigInteger("2");
        if (!two.equals(number) && BigInteger.ZERO.equals(number.mod(two))) {
            return false;
        }

        for (BigInteger i = new BigInteger("3"); i.multiply(i).compareTo(number) < 1; i = i.add(two)) {
            if (BigInteger.ZERO.equals(number.mod(i))) {
                return false;
            }
        }
        return true;
    }

    public static void showOutput(BigInteger[] arrayBig) {
        //Input total number that want to find which is modified Tribonacci number and also a prime number
        String str_input = JOptionPane.showInputDialog("Find total Number that is both \nmodified Tribonacci number and also a prime number: \n");

        int input_num = Integer.parseInt(str_input);

        String str_output = "";

        for (int i = 0; i < input_num; i++) {
            str_output = str_output + " X" + (i + 1) + " = " + arrayBig[i] + "\n";
        }

        //show output
        JOptionPane.showMessageDialog(null,
                str_output,
                "Output", JOptionPane.INFORMATION_MESSAGE);

        //show continue option
        int selectedOption = JOptionPane.showConfirmDialog(null, "Continue?", "Choose", JOptionPane.YES_NO_OPTION);

        if (selectedOption == JOptionPane.YES_OPTION) {
            showOutput(arrayBig);
        } else {
            System.exit(0);
        }
    }

    public static void main(String[] args) {

        // =============== QUESTION 2 =================
        // Sequence of natural numbers which exist in both
        // Modified Tribonacci number series [Tn = Tn-1 + Tn-2 + Tn-3] and
        // Prime number   
        // ============================================
        JFrame frame = new JFrame("Test");

        ImageIcon loading = new ImageIcon("ajax-loader.gif");
        frame.add(new JLabel("loading... ", loading, JLabel.CENTER));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 300);
        frame.setVisible(true);

        BigInteger[] arrayBig = TribonacciPrime(80);

        frame.setVisible(false);

        showOutput(arrayBig);
    }

}
