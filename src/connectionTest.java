package tttte;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

public class connectionTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	try {
			File file = new File("application.properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();

//			Enumeration enuKeys = properties.keys();
//			while (enuKeys.hasMoreElements()) {
//				String key = (String) enuKeys.nextElement();
//				String value = properties.getProperty(key);
//				System.out.println(key + ": " + value);
//			}
			
			// # MySQL 5.7
			// datasource.username: root
			// datasource.password: root
			// datasource.url: jdbc:mysql://localhost:3306/pms
			// datasource.driver: com.mysql.jdbc.Driver

			try {
				Class.forName(properties.getProperty("datasource.driver"));
			} catch (ClassNotFoundException e) {
				System.out.println("Where is your MySQL JDBC Driver?");
				e.printStackTrace();
				return;
			}

			Connection connection = null;

			try {
				connection = DriverManager.getConnection(properties.getProperty("datasource.url"),
						properties.getProperty("datasource.username"), properties.getProperty("datasource.password"));

			} catch (SQLException e) {
				System.out.println("Connection Failed! Check output console");
				e.printStackTrace();
				return;
			}

			if (connection != null) {
				System.out.println("You made it, take control your database now!");
			} else {
				System.out.println("Failed to make connection!");
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
