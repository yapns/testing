package tt;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class RemoveDuplicateMap {
    
    //TASK: to remove duplicate character from an input string
    //e.g. Input: baac, Output: bac
    //Method to use: Map
    
    public static void main(String arg[]) {

        //START define and print out input
        String str_input = "babc";
        char[] charInputArray = str_input.toCharArray();
 
        System.out.println("Input: " + str_input + "\n");
        //END define and print out input
        
        //START using map to find unique value
        Map<Integer, String> map = new LinkedHashMap<Integer, String>();

        for (int i = 1; i <= charInputArray.length; i++) {
            map.put(i, String.valueOf(charInputArray[i - 1]));
        }

        Set<String> valueSet = new TreeSet<String>(map.values());
        System.out.println("Set of unique values");
        System.out.println(valueSet);
        //END using map to find unique value
        
         
        String str_output = "";
  
        char[] char_without_sort = new char[str_input.length() + 2];

        Iterator<String> iterator = valueSet.iterator();
        Map<Integer, String> uniqueMap = new HashMap<Integer, String>();
 
        while (iterator.hasNext()) {
            String value = iterator.next();
            char c = value.charAt(0);

            str_output += value;

            for (Map.Entry<Integer, String> e : map.entrySet()) {
                if (value.equals(e.getValue()) && !uniqueMap.containsValue(value)) {
                    uniqueMap.put(e.getKey(), value);
                    char_without_sort[e.getKey()] = c;
                }
            }
        }

        System.out.println("After removing duplicate values from map");
        System.out.println(uniqueMap);
        
        //START return output with sorting
        System.out.println("\nOutput with sorting: " + str_output);
        //END return output with sorting
       
        //START return output without sorting
        str_output = "";
        for (int j = 0; j < char_without_sort.length; j++) {
            //check string if not whitespace
            if (String.valueOf(char_without_sort[j]).matches(".*\\w.*")) {
                str_output += String.valueOf(char_without_sort[j]);
            } 
        }

        System.out.println("\nOutput without sorting: " + str_output+"\n");
        //END return output without sorting
    }
}
