A palindrome is a word that reads the same backward or forward

write a function that checks if a given word is palindrome.
Character case should be ignored

For example, isPalindrome("Develeped") should return true as character case should be ignored,
resulting in "develeped", which is a palindrome since it reads the same backward or forward.



public class Palindrome {
    public static boolean isPalindrome(String word) {
      //  throw new UnsupportedOperationException("Waiting to be implemented.");

        char [] c = word.toCharArray();
        
        String strreverse = "";
        
        for (int i = word.length()-1;i >= 0;i--){
            System.out.println(c[i]);
            
           strreverse += c[i];
        }
     
        if(word.toLowerCase().equals(strreverse.toLowerCase())){
            return true;
        }else{
              return false;
        } 
    
    }
    
    public static void main(String[] args) {
        System.out.println(Palindrome.isPalindrome("Deleveled"));
    }
    
}

//======================================================================
<?php
class Palindrome
{
    public static function isPalindrome($word)
    {     
         $output="";
         
         for ( $i=strlen($word)-1;$i>=0;$i--){

          $output.=$word[$i];
        }
 
 
   //echo strtolower($output)."\n";
   
      if (strtolower($output) == strtolower($word)){
          return true;
      }else{
          return false;
      }
   }
}

echo Palindrome::isPalindrome('Deleveled');