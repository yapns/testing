package testing;

// Write a program to display the factorial of a given number, 
// using repetitive mechanism but without looping 
// Factorial of 1 is 1
// Factorial of 2 is 1 x 2 = 2
// Factorial of 3 is 1 x 2 x 3 = 6
// Factorial of 4 is 1 x 2 x 3 x 4 = 24

public class Factorial {

	public static void main(String[] args) {

		int input = 11;

		System.out.print("Factorial of " + input + " is ");
		
		int output = factorial(input);

		System.out.print(output);
	}

	public static int factorial(int input) {

		int j = 0;
		
		if (input == 1) {
			
			j = 1;
			
		} else if (input > 1) {

			System.out.print(input - (input - 1) + " x ");

			j = (input - 1) * input;

			if (input != 2) {
				System.out.print(input - (input - 2) + " x ");
			}

			j = repetitive(input - 2, j, input);
		}
		return j; 
	}

	public static int repetitive(int i, int j, int num) {

		if (i > 1) {

			System.out.print(num - (i - 1) + " x ");

			j = j * i;
			j = repetitive(i - 1, j, num);

		} else {

			System.out.print(num + " = ");
		}
		return j;

	}

}
