import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nsyap
 */
public class HanDecimalUnicode {

	public static String DecimalHan(String str_output) {

		List<String> list = new ArrayList<String>();

		char c1;

		for (int i = 0; i < str_output.length(); i++) {

			c1 = str_output.charAt(i);

			if (Character.toString(c1).equals("&")) {
				list.add(str_output.substring(i, i + 8));

				i=i + 7;
			} else {

				list.add(Character.toString(str_output.charAt(i)));
			}
		}

		String strHanChinese = "";
		String str;
		for (int j = 0; j < list.size(); j++) {

			if (list.get(j).contains("&")) {
				str = list.get(j).substring(2, list.get(j).length() - 1);

				strHanChinese += new String(Character.toChars(Integer.parseInt(str)));

			} else {
				strHanChinese += list.get(j);
			}

		}

		return strHanChinese;
	}

	public static String HanDecimal(String str_input) {
		for (int j = 0; j < str_input.length(); j++) {

			String char_at_i = Character.toString(str_input.charAt(j));

			int codepoint = str_input.codePointAt(j);

			// UnicodeHan_to_Decimal
			if (Character.UnicodeScript.of(codepoint) == Character.UnicodeScript.HAN || str_input.charAt(j)=='「' || str_input.charAt(j)=='」') {

				String decimal = Integer.toString(codepoint);

				decimal = "&#" + decimal + ";";

				str_input = str_input.replace(char_at_i, decimal);
			}
		}

		return str_input;
	}

	public static String HanUnicode(String str_input) {

		char c;
		String str_output = "";

		for (int i = 0; i < str_input.length(); i++) {

			c = str_input.charAt(i);

			if (Character.UnicodeScript.of(c) == Character.UnicodeScript.HAN || c=='「' || c=='」') {

				str_output += "\\u" + Integer.toHexString(c | 0x10000).substring(1);
			} else {
				str_output += c;
			}
		}

		return str_output;
	}

	public static String UnicodeHan(String str_output) {

		List<String> list = new ArrayList<String>();

		char c1;

		for (int i = 0; i < str_output.length(); i++) {

			c1 = str_output.charAt(i);

			if (Character.toString(c1).equals("\\")) {
				list.add(str_output.substring(i, i + 6));

				i=i + 5;
			} else {

				list.add(Character.toString(str_output.charAt(i)));
			}
		}

		String strHanChinese = "";
		String str;
		for (int j = 0; j < list.size(); j++) {

			if (list.get(j).contains("\\")) {
				str = "0x" + list.get(j).substring(2, list.get(j).length());
				strHanChinese += new String(Character.toChars(Integer.decode(str)));

			} else {
				strHanChinese += list.get(j);
			}

		}

		return strHanChinese;
	}

	public static void main(String[] args) throws IOException {

		// ENCODE
		// HanDecimal("由") - &#30001;
		// HanUnicode("由") - \u7531

		// DECODE
		// Integer.decode("0x7531") - 30001
		// Integer.toHexString(30001) - 7531
		// new String(Character.toChars(30001)) - 由

		String str_input = "「亞洲至」<br/>推廣活動 - 宣傳短片";
		System.out.println("Input  = " + str_input + "\n");

		// Han Chinese to Decimal
		System.out.println("Han to Decimal = " + HanDecimal(str_input));

		// Decimal to Han Chinese
		String strDecimal = HanDecimal(str_input);
		System.out.println("Decimal to Han = " + DecimalHan(strDecimal) + "\n");

		// Han Chinese to Unicode
		System.out.println("Han to Unicode = " + HanUnicode(str_input));

		// Unicode to Han Chinese
		String strUnicode = HanUnicode(str_input);
		System.out.println("Unicode to Han = " + UnicodeHan(strUnicode));

	}
}