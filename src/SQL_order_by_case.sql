SELECT TABLE_NAME, INDEX_TYPE from dba_indexes where index_name in (
'SYS_IL0000013281C00010$$',
'SYS_IL0000013075C00004$$',
'SYS_IL0000013067C00010$$',
'SYS_IL0000013281C00005$$',
'SYS_IL0000013422C00004$$',
'SYS_IL0000055286C00036$$',
'SYS_IL0000062780C00004$$',
'SYS_IL0000062800C00006$$',
'SYS_IL0000062817C00007$$',
'SYS_IL0000062817C00009$$',
'SYS_IL0000062827C00005$$',
'SYS_IL0000062833C00004$$',
'SYS_IL0000062839C00005$$',
'SYS_IL0000062845C00004$$',
'SYS_IL0000062851C00008$$',
'SYS_IL0000062266C00043$$',
'SYS_IL0000062266C00044$$',
'SYS_IL0000066003C00005$$',
'SYS_IL0000067081C00005$$',
'SYS_IL0000067132C00003$$',
'SYS_IL0000067341C00017$$',
'SYS_IL0000067652C00003$$',
'SYS_IL0000067773C00008$$',
'SYS_IL0000067830C00004$$',
'SYS_IL0000067840C00006$$'
) 
and INDEX_TYPE = 'LOB'
ORDER BY CASE 
WHEN index_name = 'SYS_IL0000013281C00010$$' THEN '10'
WHEN index_name = 'SYS_IL0000013075C00004$$' THEN '11'
WHEN index_name = 'SYS_IL0000013067C00010$$' THEN '12'
WHEN index_name = 'SYS_IL0000013281C00005$$' THEN '13'
WHEN index_name = 'SYS_IL0000013422C00004$$' THEN '14'
WHEN index_name = 'SYS_IL0000055286C00036$$' THEN '15'
WHEN index_name = 'SYS_IL0000062780C00004$$' THEN '16'
WHEN index_name = 'SYS_IL0000062800C00006$$' THEN '17'
WHEN index_name = 'SYS_IL0000062817C00007$$' THEN '18'
WHEN index_name = 'SYS_IL0000062817C00009$$' THEN '19'
WHEN index_name = 'SYS_IL0000062827C00005$$' THEN '20'
WHEN index_name = 'SYS_IL0000062833C00004$$' THEN '21'
WHEN index_name = 'SYS_IL0000062839C00005$$' THEN '22'
WHEN index_name = 'SYS_IL0000062845C00004$$' THEN '23'
WHEN index_name = 'SYS_IL0000062851C00008$$' THEN '24'
WHEN index_name = 'SYS_IL0000062266C00043$$' THEN '25'
WHEN index_name = 'SYS_IL0000062266C00044$$' THEN '26'
WHEN index_name = 'SYS_IL0000066003C00005$$' THEN '27'
WHEN index_name = 'SYS_IL0000067081C00005$$' THEN '28'
WHEN index_name = 'SYS_IL0000067132C00003$$' THEN '29'
WHEN index_name = 'SYS_IL0000067341C00017$$' THEN '30'
WHEN index_name = 'SYS_IL0000067652C00003$$' THEN '31'
WHEN index_name = 'SYS_IL0000067773C00008$$' THEN '32'
WHEN index_name = 'SYS_IL0000067830C00004$$' THEN '33'
WHEN index_name = 'SYS_IL0000067840C00006$$' THEN '34'
END ASC  
